'use strict'
  var topic_model = require('../model/topic_model.js');
  var chatpush_model = require('../model/ChatPush_model.js');
  var chat_model = require('.././model/Chat_model.js');
  var task_controller = require('../controller/task_controller.js');
  var tag_controller = require('../controller/tag_controller.js');
  var user_controller = require('../controller/User_controller.js');
  var file_controller = require('../controller/file_controller.js');
  var utility_model = require('../model/utility_model.js');
  var user_model = require('../model/User_model.js');
  var utility_controller = require('../controller/utility_controller.js');
  var comment_model = require('../model/comment_model.js');
  var resp = require('../controller/utility_controller.js');
  var async = require("async");
  var find = require("array-find");

  module.exports.topic_get = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}


      var data = body.data; //{"topic_id":data.data.topic_id,"user_id":data.data.user_id};
       console.log(data);
      topic_model.topic_get(data,function(err,rpta){
        console.log("err");
        console.log(err);
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}

        var response = {"resp_cod":0,"data":{}};

        if(!err && rpta ){
          async.mapValues(rpta, eachtopic, function(err,rpta){
            console.log("--response--");
            console.log(response);
            response = {"resp_cod":0,"data":rpta};
            utility_controller.sendres(res,response);return false;
          });
        }else{
          utility_controller.sendres(res,{"resp_cod":0,"data":{}});return false;
        }
      });
    });
  }

  function eachtopic(value, key, callback){
/*
    if(key == "tag"){ //se obtiene datos de tag
      if(value && value.length > 0){
        var arr = [];
        value.forEach(function(values,index){
          arr.push({"tag_id":values});
        });
        async.mapSeries(arr, tag_controller.addindextag, function(error,arr){
          var tmp = !error ?  arr : [];
          callback(error,tmp);
        });
      }else{
        callback(null,[]);
      }
    }
    if(key == "task"){ //se obtiene datos de task
      if(value && value.length > 0){
        var arr = [];
        value.forEach(function(values,index){
          arr.push({"_id":values});
        });
        async.mapSeries(arr, task_controller.addindextask, function(error,arr){
          var task = !error ?  arr : [];
          callback(error,task);
        });
      }else{
        callback(null,[]);
      }
    }else
    if(key == "user"){ //se obtiene datos de usuario
      if(value && value.length > 0){
        var arr = [];
        // console.log(value);
        value.forEach(function(values,index){
          arr.push({"user_id":values});
        });
        async.mapSeries(arr, user_controller.addindexuser, function(error,arr){
          var user = !error ?  arr : [];
          callback(error,user);
        });
      }else{
        callback(null,[]);
      }
    }else{
      callback(null,value);
    }*/
   callback(null,value);
}

  module.exports.topic_add = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
      var response = {"code":10};

      var data = body.data;

      var dat = {
        "creator_id": data.user_id,
        "name": data.name,
        "description": data.description,
        "photo": data.photo ? data.photo  : "",
        "tag": data.tag
       };
      
      topic_model.topic_add(dat,function(error,datainsert){
        console.log("mmmmmmmmmmmmmm");
        console.log(datainsert);
        console.log(error);
        var data = {"resp_cod":10,"data":{}};
        if(datainsert.result.ok == 1){
          var dat = datainsert.ops[0];
          dat.topic_id = dat._id;
          data = {"resp_cod":0,"data":dat};
        }
        utility_controller.sendres(res,data);
      });    
    });
}


  module.exports.topic_related = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
      var data = body.data;

      var action = data.action;

      topic_model.topic_get(data,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}
        var topic_related = result.topic_related ? result.topic_related : [];

        if(action == "0"){
          var newrelated = {"user_id":data.user_id,"topic_id":data.new_related_topic_id,
                          "date_create":Date.now()};
          topic_related.push(newrelated);
        }else{
          var aux = topic_related;
          aux.forEach(function(value,index){
            if(value.topic_id == data.new_related_topic_id){
               topic_related.splice(index, 1);
            }
          })
        }

        result.topic_related = topic_related;
        result.date_updated = Date.now();
        topic_model.update(data.topic_id,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data":topic_related});
        });
      });
    });
  }

  module.exports.topic_attach_file = function(req, res){

    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
      var data = body.data; // se obtiene los datos que estan en indice data dentro del objecto enviado por cliente
      console.log("2");
      var action = data.action;

      topic_model.topic_get(data,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        console.log("3");
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}
        // Si no hay error y el id existe.. pasa a crear la variable..
        var file = result.file && result.file.constructor === Array ? result.file : [];
        console.log("4");
        console.log(file);
        if(action == "0"){
          var newattachfile = {"file_id": data.file_id,"user_id":data.user_id,"date_updated":Date.now()};
          file.push(newattachfile);
        }else{
          var aux = file;
          aux.forEach(function(value,index){
            if(value.file_id == data.file_id){
               file.splice(index, 1);
            }
          })
        }
        console.log("5");
        result.file = file;
        result.date_updated = Date.now();
        topic_model.update(data.topic_id,result,function(err,resultedit){
          console.log(err);
          console.log(resultedit);
          utility_controller.sendres(res,{"resp_cod":0,"data": file});
          console.log("6");
        });
      });
    });
  }

  module.exports.topic_update_general = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var dat = {
        "topic_id": body.data.topic_id
      };

      topic_model.topic_get(dat,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}
        // Si no hay error y el id existe.. pasa a crear la variable..
        var data = body.data;

        // result.creator_id = data.user_id && data.user_id.length > 0 ? data.user_id : result.creator_id;
        result.name = data.name && data.name.length > 0 ? data.name : result.name;
        result.description = data.description && data.description.length > 0 ? data.description : result.description;
        result.photo = data.photo ? data.photo : result.photo;
        result.status = data.status && data.status.length > 0 ? data.status : result.status;
        result.tag = data.tag ? data.tag : result.tag ;
        result.file = data.file ? data.file : result.file;

        var user = result.user ? result.user : [];
        user.forEach(function(value,index){
          sendpush_updategeneral(req,res,data,value.user_id);
        });

        result.date_updated = Date.now();
        topic_model.update(data.topic_id,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data": result});
        });
      });
    });
  }

  function sendpush_updategeneral(req,res,datacustomer,user_id){
    console.log("--sendpush---");
    console.log(datacustomer);
    var value = {"user_id":user_id};
    console.log(value);
    user_model.userbyid(value,function(err,response){
      console.log("--mm----");
      console.log(response);
      //dato que le llegara al usuario en el push
      var data = {"sender_id":datacustomer.user_id,"action_type":0,
        "data":{"topic_id":datacustomer.topic_id} 
      };
      console.log(response.topic_device.length);
      var id_device_topic = response.topic_device[response.topic_device.length-1].id_device_topic;
      console.log("id_device_topic ----- "+id_device_topic);
      if(id_device_topic ){
        console.log('**********************************'); 
        console.log(response.id_device_topic);
        chatpush_model.PushNotification(req,res,data,id_device_topic);
      }
    })
  }

  module.exports.topic_update_users = function(req, res){
    var body = req.body;
    //console.log(body);
    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
      var data = body.data;
      // var action = data.data.action;
      var dat = {
        "topic_id": data.topic_id
      };

      topic_model.topic_get(dat,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}

        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}
        // Si no hay error y el id existe.. pasa a crear la variable..

        // var user = result.user ? result.user : [];
        var user = [];
        var userpost = data.user ? data.user : [];


        if(userpost && userpost.length > 0){
         // console.log("**********5555***********");
         // console.log(userpost);
          userpost.forEach(function(value,index){
            var tmp = value;
            sendpush_newuser(req,res,data,value.user_id);

            tmp.date_create = Date.now();
            user.push(tmp)
          });
        }

        result.user = user;
        result.date_updated = Date.now();
        topic_model.update(data.topic_id,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data": result});
        });
      });
    });
  }

  function sendpush_newuser(req,res,datacustomer,user_id){
    console.log("--sendpush_newuser---");
    //console.log(datacustomer);
    var value = {"user_id":user_id};
    console.log(value);
    user_model.userbyid(value,function(err,response){
      console.log("--mm----");
      //console.log(response);
      //dato que le llegara al usuario en el push
      var data = {"sender_id":datacustomer.user_id,"action_type":1,
        "data":{"topic_id":datacustomer.topic_id} 
      };
      console.log(response.topic_device.length);
      var id_device_topic = response.topic_device[response.topic_device.length-1].id_device_topic;
      console.log("id_device_topic ----- "+id_device_topic);
      if(id_device_topic ){
        console.log('**********************************'); 
        console.log(response.id_device_topic);
        chatpush_model.PushNotification(req,res,data,id_device_topic);
      }
    })
  }

  module.exports.topic_comment = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var data = body.data;

      comment_model.comment_add_topic(data,function(err,datainsert){
        //si hay algún error, retorna al cliente el error
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        console.log(err);
        console.log(datainsert);
        if(datainsert.result.ok != 1){
          if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        }
        var arrinsert = datainsert.ops[0];
        var comment_id = arrinsert._id;


        topic_model.topic_get(data,function(err,result){
          if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
          if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}

          var arrcomment = result.comment ? result.comment : [];
          arrcomment.push(comment_id);

          result.comment = arrcomment;
          result.date_updated = Date.now();
          topic_model.update(data.topic_id,result,function(err,resultedit){
            utility_controller.sendres(res,{"resp_cod":0,"data":{"comment_id":comment_id,"date":result.date_updated}});
          });
        });

      });

    });
  }

  module.exports.topic_suscribe = function (req, res){
    var body = req.body; //se obtiene datos enviados por usuario

    utility_model.checkbasicdata(body,function(err,datasession){ //se valida apikey y sessión key
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var data = body.data; //se obtiene los datos del indice data, enviado por el usuario
      var notification = data.is_notifications_enabled ? data.is_notifications_enabled : false;

      topic_model.topic_get(data,function(err,result){ //se obtiene datos del topic por id
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;} //si hay error entonces se devuelve error a usuario
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;} //si devuelve vacio, osea no existe topic

        var arruser = result.user ? result.user : []; //se extrae el indice user, del topic, esto es un array de usuario
        var tmp = arruser;

        tmp.forEach(function(value,index){ //se recorre todo el array de usuarios del topic, y se busca el que envia el usario
          if(value.user_id == data.user_id){
            var userobj = value;
            userobj.notification = notification;
            arruser[index] = userobj;
          }
        });

        result.user = arruser;
        result.date_updated = Date.now();
        topic_model.update(data.topic_id,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data":{"is_notifications_enabled":notification}});
        });
      });
    });
  }

  module.exports.user_topics = function(req, res){
    var data = {"topic_userId": req.params.id};

    topic_model.user_topics(data, function(err, data){
      var rpta = {"code":1,"data":data};
      res.setHeader('Content-Type', 'application/json');
      res.status(200).jsonp(rpta);
    });
  }

  module.exports.topic_update_task = function(data, callback){
      // var topic_id = data.topic_id;

      topic_model.topic_get(data,function(err,result){
        if(err){callback(err,result);return;}

        var user = [];
        console.log("**********")
        console.log(result);
        var task = result.task ? result.task : [] ;

        task.push(data.task_id);

        result.task = task;
        result.date_updated = Date.now();
        topic_model.update(data.topic_id,result,function(err,resultedit){
          callback(err,resultedit);
        });
      });
    }

  module.exports.topic_get_user = function(req, res){
    var body = req.body;
    
    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
      var data = body.data;
      var dat = {
        "topic_id": data.topic_id
      };

      topic_model.topic_get(dat,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}

        var user = result.user ? result.user : [];
        
        async.mapSeries(user, eachtopicuser, function(err,response){
          if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
          result.user = response;
          utility_controller.sendres(res,{"resp_cod":0,"data": result}); 
        });
      });
    });
  }

  function eachtopicuser(value,callback){
    var data = value;
     var photo = data.photo;
     var fullUrl = "http://107.182.239.51:8081";
     var ruta = fullUrl + photo;

    user_model.userbyid(value,function(err,response){
      if(err || !response || Object.keys(response).length == 0){callback(err,{});return;}
      data.name = !err && response.name ? response.name : "";
      data.identifier = !err && response.extension ? response.extension : "";
      data.photo = !err && fullUrl + response.photo ? fullUrl + response.photo : "";
      callback(err,data);
    });
  }

  module.exports.topic_device = function(req, res){

    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
      var data = body.data; 
      var action = data.action;

      topic_model.topic_iddevice(data,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}
        
        var topic_device = result.topic_device && result.topic_device.constructor === Array ? result.topic_device : [];
        if(action === "0"){
          var newdevice = {"id_device_topic": data.id_device_topic,"serial_number":data.serial_number,"user_id":data.user_id,"date_updated":Date.now()};
          var existdevice = find(topic_device, function(device) {
            return device.serial_number === data.serial_number;
          });
          if (existdevice) {
            topic_device.forEach(function(value,index){
              if(value.serial_number === data.serial_number){
                topic_device[index] = newdevice;
              }
            });
          } else {
            topic_device.push(newdevice);
          }
        } else {
          var aux = topic_device;
          aux.forEach(function(value,index){
            if(value.id_device_topic === data.id_device_topic){
              topic_device.splice(index, 1);
            }
          })
        }
        result.topic_device = topic_device;
        result.date_updated = Date.now();
        topic_model.update_device(data.user_id,data.serial_number,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data": topic_device});
        });
      });
    });
  }

module.exports.comment_get_topic = function(req, res){
  var body = req.body;

  utility_model.checkbasicdata(body,function(err, datasession){
    if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = {
      "comment_id": body.data.comment_id
    };

    comment_model.comment_get_topic(data,function(err,result){
      console.log("***marielb****");
      console.log(result);
      console.log(err);
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": result});

    });
  });
}

module.exports.topic_history = function(req,res){

  console.log("esto es una prueba 2");

  var body = req.body;

  utility_model.checkbasicdata(body,function(err, datasession){
  if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = body.data;
    
    topic_model.topic_history(data, function(err, results){
      
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!results){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": results});
    });            
  });    
}

module.exports.comment_topic_history = function(req,res){

  var body = req.body;

  utility_model.checkbasicdata(body,function(err, datasession){
  if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = body.data;
    
    comment_model.comment_topic_history(data, function(err, results){
      
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!results){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": results});
    });            
  });    
}