var task_model = require('.././model/task_model.js');
var topic_model = require('../model/topic_model.js');
var task_controller = require('../controller/task_controller.js');
var tag_controller = require('../controller/tag_controller.js');
var user_controller = require('../controller/User_controller.js');
var file_controller = require('../controller/file_controller.js');
var utility_model = require('../model/utility_model.js');
var utility_controller = require('../controller/utility_controller.js');
var topic_controller = require('../controller/topic_controller.js');
var comment_model = require('../model/comment_model.js');
var async = require("async");

module.exports.addindextask = function(data,callback){
	getbyid(data,callback);
}

function getbyid(data,callback){
	var dat = data;
	task_model.taskbyid(dat,function(err,rpta){
		callback(err,rpta);
	});
}

module.exports.task_add = function(req, res){
	var body = req.body;

	utility_model.checkbasicdata(body,function(err,datasession){
		if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

		var data = body.data;

		task_model.task_add(data,function(error,datainsert){
			console.log("mmmmmmmmmmmmmm");
			console.log(datainsert);
			console.log(error);
			var response = {"resp_cod":10,"data":{}};
			if(datainsert.result.ok == 1){
				var dat = datainsert.ops[0];
				dat.task_id = dat._id;
        var topicupdate = {"task_id":dat._id,"topic_id":data.topic_id};
        console.log("------ topicupdate");
        console.log(topicupdate);

        topic_controller.topic_update_task(topicupdate,function(){});

				response = {"resp_cod":0,"data":dat};
			}
			utility_controller.sendres(res,response);
		});
	});
}

module.exports.task_get = function(req, res){
  var body = req.body;

  utility_model.checkbasicdata(body,function(err, datasession){
    if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = {
      "task_id": body.data.task_id
    };

    task_model.task_get(data,function(err,result){
      console.log("***marielb****");
      // console.log(result);
      // console.log(err);
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": result});

    });
  });
}

  module.exports.task_update = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var dat = {
        "task_id": body.data.task_id
      };

      task_model.task_get(dat,function(err,result){
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}
        // Si no hay error y el id existe.. pasa a crear la variable..
        var data = body.data;

        result.user_id = data.user_id && data.user_id.length > 0 ? data.user_id : result.user_id;
        result.name = data.name && data.name.length > 0 ? data.name : result.name;
        result.description = data.description && data.description.length > 0 ? data.description : result.description;
        result.end_date = data.end_date ? data.end_date : 0,
        result.assigned_to = data.assigned_to ? data.assigned_to : result.assigned_to ;

        console.log("************");
        console.log(data);
		    console.log(result);

        task_model.update(data.task_id,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data": result});
        });
      });
    });
  }

    module.exports.task_mark = function (req, res){
    var body = req.body; //se obtiene datos enviados por usuario

    utility_model.checkbasicdata(body,function(err,datasession){ //se valida apikey y sessión key
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var dat = {
        "task_id": body.data.task_id
      };

      task_model.task_get(dat,function(err,result){ //se obtiene datos del task por id
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;} //si hay error entonces se devuelve error a usuario
        if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;} //si devuelve vacio, osea no existe task

        var data = body.data; //se extraen los datos que envia el usuario

        result.date_updated = Date.now();
        result.is_completed = data.is_completed ? data.is_completed : false ;

        console.log("************");
        console.log(data);
		    console.log(result);

        task_model.update(data.task_id,result,function(err,resultedit){
          utility_controller.sendres(res,{"resp_cod":0,"data":{"is_completed": result.is_completed}});
        });
      });
    });
  }

    module.exports.task_comment = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var data = body.data;

      comment_model.comment_add_task(data,function(err,datainsert){
        //si hay algún error, retorna al cliente el error
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        console.log(err);
        console.log(datainsert);
        if(datainsert.result.ok != 1){
          if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
        }
        var arrinsert = datainsert.ops[0];
        var comment_id = arrinsert._id;


        task_model.task_get(data,function(err,result){
          if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}
          if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}

          var arrcomment = result.comment ? result.comment : [];
          arrcomment.push(comment_id);

          result.comment = arrcomment;
          result.date_updated = Date.now();
          task_model.update(data.task_id,result,function(err,resultedit){
            utility_controller.sendres(res,{"resp_cod":0,"data":{"comment_id":comment_id,"date":result.date_updated}});
          });
        });
      });
    });
  }

  module.exports.comment_get_task = function(req, res){
  var body = req.body;

  utility_model.checkbasicdata(body,function(err, datasession){
    if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = {
      "comment_id": body.data.comment_id
    };

    comment_model.comment_get_task(data,function(err,result){
      console.log("***marielb****");
      console.log(result);
      console.log(err);
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!result){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": result});

    });
  });
}

module.exports.task_history = function(req,res){

  var body = req.body;


  utility_model.checkbasicdata(body,function(err, datasession){
  if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = body.data;
    
    task_model.task_history(data, function(err, results){
      
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!results){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": results});
    });            
  });    
}

module.exports.comment_task_history = function(req,res){

  var body = req.body;


  utility_model.checkbasicdata(body,function(err, datasession){
  if(err){utility_controller.sendres(res,{"resp_cod":datasession.code}); return false;}

    var data = body.data;
    
    comment_model.comment_task_history(data, function(err, results){
      
      if(err){utility_controller.sendres(res,{"resp_cod":10}); return false;}
      if(!results){utility_controller.sendres(res,{"resp_cod":4});return false;}

        utility_controller.sendres(res,{"resp_cod":0,"data": results});
    });            
  });    
}