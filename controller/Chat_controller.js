//var mod = require('.././model/ChatPush_model.js');
var chat_model = require('.././model/Chat_model.js');
var async = require("async");
var Broadcast_model = require('.././model/Broadcast_model.js');
var ChatAdd_model = require('.././model/ChatAdd_model.js');
var ChatUser_model = require('.././model/ChatUser_model.js');
var utility_controller = require('../controller/utility_controller.js');
var find = require("array-find");
var utility_model = require('../model/utility_model.js');
var ccc_model = require('../model/ccc_model.js');


module.exports.newuser = function(data,callback){

		var threads_id = data.threads_id;
		var status = data.status;

     ChatUser_model.chatUserWeb(data,null,function(rpta){
		
		 var data = {"body":{"_id":threads_id , "status": status}};
		
		 var rs = chat_model.userIdRooms(data, function(rpta){
			  console.log("rpta de sala por usuario: ",rpta);
			  
			  

			 async.mapSeries(rpta,setlastmessage,function(error,data){
				 var rpta3 = []; 
   
					if(data.length>0){		  
						 async.mapSeries(data,getmembers,function(error,data2){
									   
									 console.log("TOTAL: ");
									console.log(error);
									 callback(data2);
									  // console.log("----------");
						 }); 
					}else{
						callback([]);
					}
			 })
					 
			 });		
				 //console.log(data);
		 }) 			 	
}

module.exports.getroombystatus = function(req, res){

    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var data = body.data; 

      ccc_model.getroombystatus(data,function(err,rpta){

        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}

          console.log("**********rpta");
          console.log(rpta);
          async.mapSeries(rpta.data,setlastmessage,function(error,data){

				 	var rpta3 = []; 

            if(!err && data){      
              
             async.mapSeries(data,getmembers,function(error,data2){
              
		          console.log("-----------------TOTAL: ---------------------");
              console.log(data2);
              utility_controller.sendres(res,data2);return false;
             }); 
          }else{
          utility_controller.sendres(res,rpta);return false;
        }
      })
      });
    });
}

function getmembers(value,callback){
  console.log("Miembros: ")
  console.log(value);
  console.log("members: ")
  console.log(value.members);
   var val = value;
   //var miembros = value.members || value.user_id;
   
   if(value.members){
     async.mapSeries(value.members ,getmemberstwo,function(error,data2){
       console.log("TraerMiembros: ");
       console.log(data2); 
         val.members = data2;
         callback(error,val);
     });
   }else{
     
     val.members = [];
     callback(null,val);
   } 
}

function getmemberstwo(value,callback ){
   console.log("MEBERETWO: ");
   console.log(value);
  
   var data = {"_id":value};
   var rs2 = chat_model.userbyid(data, function(rpta2){
     var aux = rpta2 ? rpta2 : {};
    console.log("AUXM2: ")
    console.log(aux);
    callback(null,aux);
   });
}

module.exports.getallmessage = function(data,callback){
	
	chat_model.roomsxid (data,function(rptaroom){
		
		// console.log(rptaroom);
		  var datarooms = rptaroom;
		  
		  chat_model.getAllMessagexRooms(data,function(rpta){
		 
		 
			async.mapSeries(rpta,formatgetallmessage,function(error,messagewithuser){
				console.log("Todos mensajes");
				callback(messagewithuser);
			});
		 
		 })
	 })
}
module.exports.deletSockets = function(req,callback){
	
	 console.log(req);
	 var rs2 = chat_model.deletSocket(req,callback);
	
}

//*********************Marielb************//

module.exports.changestatusrooms = function(data, callback){

	var data1 = data.roomid;
	chat_model.changestatusrooms(data1,data,function(err,res){
    callback(err, res);
  });
}

//*********************Marielb************//

module.exports.getMessageId = function(data,callback){
	 
	 chat_model.messageId(data,function(rptaroom){
		// var aux = rptaroom ? rptaroom : {};
		 console.log(rptaroom);
		 if(rptaroom){
			 
			 formatgetallmessage(rptaroom,function(err,message){
				 console.log("message");
				console.log(message);
			 });
		 }
	 })
}	

module.exports.getMessageIdAux = function(data,callback){
	 chat_model.messageId(data,function(rptaroom){
		 //callback(rptaroom);
		 console.log(rptaroom);
		 formatgetallmessage(rptaroom,function(error,rpta){
			callback(rpta);
		 })
	 })
}	

function formatgetallmessage(data,callback){

	var aux = data;
	var dat = {"_id":data.user_id};
	var rs2 = ChatUser_model.userbyid(dat, function(user){
		 var tmp = user ? user : {};
		aux.user = tmp;
		//console.log(aux);
		callback(null,aux);
	 });
}

module.exports.getCustomers = function(req,callback){
	 console.log('Controller: GetCustomers');
	//console.log(data.body);
	 var rs2 = ChatUser_model.getCustomer(req,callback);
}

module.exports.grabarchatcont = function(data,callback){

	console.log('.-.-.-.-.-.-................CANALFK...................----------------------')
	console.log(data.canalfk);
	console.log('.-.-.-.-.-.-...................................----------------------')
	
	if(data.canalfk!='app'){
	
		var data = {"body":data};
		ChatAdd_model.GrabarChat (data,null,function(rptamsg){
			 
			 var id = rptamsg.data ? rptamsg.data.insertedIds[0] : rptamsg.insertedIds[0];
			 
			 getmessageid(id,function(error,data){
			 
				 formatgetallmessage(data,function(err,message){
					// console.log("-----message-----");
					callback(message);
				 });
			 });
		})
	
	}else{ //POR APP
		
		 var mod = require('../model/Push_notifications_model.js');
		 
		 data.canalfk='appfinal';
		 var data = {"body":data};
		 
			 //SE ENVIA PUSH FK == APP
			 mod.EnviarPush(data,null,function(rs){
			 })
	}
}

function setlastmessage(value,callback){

	 var val = value;
	 // console.log("********val12*********");
  //   console.log(val);
	 var rs = chat_model.messagexrooms(val, function(value){
		var aux = value ? value : {};
		val.lastmessage = aux;
		// console.log("********val*********");
  //   console.log(val);
		callback(null,val);
	 });
}

function getmessageid(value,callback){
	//console.log("getmessageid");
	//console.log(value);
	 var data = {"_id":value};
	  
	 var rs2 = chat_model.messageId(data, function(rpta2){
		 var aux = rpta2 ? rpta2 : {};
		// console.log("AUX MESSAGE: ")
		//console.log(aux);
		callback(null,aux);
	 });
	
}
//module.exports.broadcastRemove=broadcastRemove;

