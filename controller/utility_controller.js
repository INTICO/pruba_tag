'use strict'
var fs = require('fs');

module.exports.sendres = function(res,response){
	res.setHeader('Content-Type', 'application/json');
	res.status(200).jsonp(response);
}

// module.exports.savefile = function(url,dataString,callback){
// 	var imageBuffer = decodeBase64Image(dataString);
// 	fs.writeFile(url, imageBuffer.data, callback);
// }

function decodeBase64Image(dataString) {
  var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
    response = {};

  if (matches.length !== 3) {
    return new Error('Invalid input string');
  }

  response.type = matches[1];
  response.data = new Buffer(matches[2], 'base64');
  console.log(response);
  return response;
}

module.exports.savefile = function(req,base64,callback,type){

  var extention = ".png"; //nombre de la imagen

  if(type == "3"){
    extention = ".mp3";
  }else if (type == "4"){
    extention = ".mp4";
  }else if(type == "5"){
    extention = ".pdf";
  }else if(type == "6"){
    extention = ".doc";
  }else if(type == "7"){
    extention = ".rar"
  }else if(type == "8"){
    extention = ".zip"
  }
   
   var imageName = (new Date).getTime() + extention
   var fullUrl = req.protocol + '://' + req.get('host'); //ip de servidor
   var newPath = __dirname + "/../assets/topic/"+ imageName;
   var url = fullUrl + "/api/v1/assets_topic/" + imageName;

   //var base64c = base64.replace(/^data:image\/png;base64,/hjkhjk, "");
   var aux = base64.split(',');

   var base64c = aux.length == 2 ? aux[1] : aux[0];
   //console.log(base64c);
   base64c = base64c.replace(/\s/g, "");
   fs.writeFile(newPath, new Buffer(base64c, "base64"), function (err,response) {
    var data = {"url":url,"patch":newPath,"imageName":imageName};
      callback(err,data);                                                                                       
   });
}

