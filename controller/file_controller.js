'use strict'
var utility_model = require('.././model/utility_model.js');
var file_model = require('.././model/file_model.js');
var resp = require('../controller/utility_controller.js');
var topic_model = require('../model/topic_model.js');
var topic_controller = require('../controller/topic_controller.js');
var task_controller = require('../controller/task_controller.js');
var tag_controller = require('../controller/tag_controller.js');
var user_controller = require('../controller/User_controller.js');
var utility_controller = require('../controller/utility_controller.js');
var async = require("async");

module.exports.file_upload = function(req,res){
	var body = req.body;
  utility_model.checkbasicdata(body,function(err,datasession){
    if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}
    var response = {"code":10};
    var data = body.data;

    if(!data.type || data.content.length < 10 ){
    	resp.sendres(res,response);return false;
    }

    console.log(data.content);
    resp.savefile(req,data.content,function(err,rptafile){
    	if(err){resp.sendres(res,response);return false};

		    var dat = {
		      "name_external": data.name,
		      "name": rptafile.imageName,
		      "url": rptafile.url,
		      "type": data.type
		    };
        

		    file_model.file_add(dat,function(err,datainsert){

	        var data = {"resp_cod":10,"data":{}};
	        if(datainsert.result.ok == 1){
	          var dat = datainsert.ops[0];
	          dat.file_id = dat._id;
	          data = {"resp_cod":0,"data":dat};
	        }
	        resp.sendres(res,data);
		    })
    },data.type);

  });
}



module.exports.saveimages = function(data,callback){
  var base64 = data.content;

  var milliseconds = (new Date).getTime();
  var name = milliseconds+ ".png";
  var link = "http://107.182.239.51:8081";
  var extra = "/api/v1/assets_topic/";
  var newPath = __dirname + "/../"+extra+name;
  resp.savefile(newPath,base64,function(err,rpta){
    if(err){callback(err,rpta);return;} 

    var dat = {"name":name,"name_external":"","url":link+extra+name,"type":1};
      file_model.file_add(dat,function(err,datainsert){
        var response = !err && datainsert.result.ok == 1 ? datainsert.ops[0] : {};
        callback(err,response);

      })


  });
}

  
module.exports.file_get = function(req, res){
    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}


      var data = body.data; //{"topic_id":data.data.topic_id,"user_id":data.data.user_id};
       console.log(data);
      file_model.file_get(data,function(err,rpta){
        console.log("err");
        console.log(err);
        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}

        var response = {"resp_cod":0,"data":{}};
        utility_controller.sendres(res,{"resp_cod":0,"data":rpta});return false;

      });
    });
  }