var mod = require('.././model/ChatPush_model.js');
var mod2 = require('.././model/Chat_model.js');
var mod3 = require('.././model/session_model.js');
var mod4 = require('.././model/Broadcast_model.js');
var mod5 = require('.././model/ChatAdd_model.js');

function ValidarSessionUser(req,res,Callback){

mod.ValidateSessionUsuario(req,res,Callback)

}

function ValidarCustomer(req,res,Callback){

mod.ValidarCustomer(req,res,Callback)

}


function GrabarChat(req,res){
	console.log("Controller: GrabarChat");
	mod5.GrabarChat(req,res)
}


function broadcastSuscribe(req,res){
	console.log("Controller: broadcastSuscribe");
	rs=mod2.broadcastSuscribe(req,res,ExtraerUsuarios);
}

function broadcastUnsuscribe(req,res){
	console.log("Controller: broadcastUnsuscribe");
	rs=mod4.broadcastUnsuscribe(req,res,ExtraerUsuarios);	
}


function broadcastRemove(req,res){
	console.log("Controller: broadcastRemove");
	rs=mod4.broadcastRemove(req,res,ExtraerUsuarios);
	
}

						
function ExtraerUsuarios(req,res,data,user_id,callback){
	console.log("Controller: ExtraerUsuarios");
	rs=mod2.ExtraerUsuarios(req,res,data,user_id,ExtraerChatsEvents);
	
}


function ExtraerChatsEvents(req,res,data){
	console.log("Controller: ExtraerChatsEvents");
	rs=mod2.ExtraerChatsEvents(req,res,data,PushNotification);
}


function GrabarChatBroad(req,res){
	console.log("Controller: GrabarChat");
	mod2.grabarBroadMessage(req,res)
}

// function GrabarChatBroadcast(req,res){
	// console.log("Controller: GrabarChatBroadcast");
	// mod2.grabarBroadMessage(req,res)
// }



function deleteSession(req,res){
	console.log("Controller: deleteSession");
	mod2.deleteSession(req,res);
}


function generateImage(req,res){ //Funcion de img 
	console.log("Controller: GImage");
	mod3.generateImage(req,res,GrabarChat)
}

function generateImageTalks(req,res){ //Funcion de img 
	console.log("Controller: GImage");
	mod3.generateImage(req,res,GrabarChatBroad)
}


function GenerateImageChatUpdate(req,res){ //Funcion de img 
	console.log("Controller: GImage");
	var chat_image = req.body.data.photo;

	if(chat_image==='' || chat_image===undefined){
	
		 mod.ChatUpdate(req,res,'',PushNotification)

	}else{
		 mod3.generateImage(req,res,ChatUpdateImagen)
	}
}


function BroadcastUpdate(req,res){ 
	 console.log("Controller: ChatUpdate");
	
	var chat_image = req.body.data.photo;

     if(chat_image==='' || chat_image===undefined){
	
		 mod.BroadcastUpdate(req,res,'',PushNotification)

    }else{
	    mod3.generateImage(req,res,BroadcastUpdateImagen)
	}
}

function ChatUpdateImagen(req,res,photo){

	console.log("Controller: ChatUpdateImagen");
	mod.ChatUpdateImagen(req,res,photo,PushNotification)

}

function BroadcastUpdateImagen(req,res,photo){

	console.log("Controller: ChatUpdateImagen");
	mod.BroadUpdateImagen(req,res,photo,PushNotification)
}



function ChatGetInformation(req,res){

	console.log("Controller: ChatGetInformation");
	mod.ChatGetInformation(req,res)

}

function PushNotification(req,res,data,idDevice){
	 console.log("Controller: PushNotification");
	 
	// console.log("Data Push:" + req.body.data);
	 
	 mod.PushNotification(req,res,data,idDevice)	
	 
}




module.exports.ChatGetInformation = ChatGetInformation;
module.exports.GenerateImageChatUpdate = GenerateImageChatUpdate;
module.exports.ValidarSessionUser = ValidarSessionUser;
module.exports.ChatUpdateImagen = ChatUpdateImagen;
module.exports.GrabarChat = GrabarChat;
module.exports.deleteSession = deleteSession;
module.exports.BroadcastUpdate=BroadcastUpdate;
module.exports.ValidarCustomer=ValidarCustomer;
module.exports.broadcastUnsuscribe=broadcastUnsuscribe;
module.exports.broadcastSuscribe=broadcastSuscribe;
module.exports.broadcastRemove=broadcastRemove;

