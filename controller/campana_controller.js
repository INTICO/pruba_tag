var mod = require('.././model/campana_model.js');
var mod_customer = require('.././model/customer_model.js');
var mod_subscribe = require('.././model/subscribe_model.js');
var mod_group = require('.././model/group_model.js');
var mod_mail = require('.././model/mailing_model.js');
var mod_sms = require('.././model/sms_model.js');

var request  = require('request');

function RegisterCampana(req,res){
	console.log('Constroller: RegisterCampana');
	rs=mod_customer.validate_customer(req,res,RegisterCampanaFK);
}
function RegisterCampanaFK(req,res){
	console.log('Constroller: RegisterCampanaFK');
	rs=mod.RegisterCampana(req,res,ListarFiltroSuscribers);
}

function ListarFiltroSuscribers(req,res){
	console.log('Constroller: ListarFiltroSuscribers');
	rs=mod_subscribe.ListarFiltroSuscribers(req,res,BoletinCod);
}

function BoletinCod(req,res){
	console.log('Constroller: BoletinCod');
	rs=mod_mail.BoletinCod(req,res,RegistrarGroup);
}

function RegistrarGroup(req,res){
	console.log('Constroller: RegistrarGroup');
	//rs=mod_group.RegistrarGroup(req,res,RegistrarSuscriber);
	rs=mod_group.RegistrarGroup(req,res,EnviarMailing);
	
}
function RegistrarSuscriber(req,res){
	console.log('Constroller: RegistrarSuscriber');
	
	rs=mod_subscribe.RegistrarSuscriber(req,res,EnviarMailing);
}	


function EnviarMailing(req,res){
	console.log('Constroller: EnviarMailing');
	rs=mod_mail.SendmailingBulk(req,res,EnviarSms);
}
	
function EnviarSms(req,res){
	console.log('Constroller: EnviarSms');
	rs=mod_sms.send_sms_bulk(req,res);
}
	

	

module.exports.RegisterCampana = RegisterCampana;

