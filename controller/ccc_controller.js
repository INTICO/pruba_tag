'use strict'

var ccc_model = require('../model/ccc_model.js');

  var utility_model = require('../model/utility_model.js');
  var utility_controller = require('../controller/utility_controller.js');

  var async = require("async");
  var find = require("array-find");

module.exports.getroombystatus = function(req, res){

    var body = req.body;

    utility_model.checkbasicdata(body,function(err,datasession){
      if(err){utility_controller.sendres(res,{"resp_cod":datasession.code});return false;}

      var data = body.data; 

      ccc_model.getroombystatus(data,function(err,rpta){

        if(err){utility_controller.sendres(res,{"resp_cod":10});return false;}

              console.log("**********rpta");
              console.log(rpta);
            if(!err && rpta){      
              
             async.mapSeries(rpta.data,getmembers,function(error,data2){
              
		          console.log("-----------------TOTAL: ---------------------");
              console.log(data2);
              utility_controller.sendres(res,data2);return false;
             }); 
          }else{
          utility_controller.sendres(res,rpta);return false;
        }
      });
    });
}
