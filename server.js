

// //PM2
// var pmx = require('pmx').init();

//NODE SOCKETS
var bodyParser = require('body-parser') //npm install body-parser
var express  = require('express');

var app = express();
var fs = require('fs');
const cors = require('cors'); // sudo npm install cors
var net = require("net");

/////CONTROLLERS////
var chat_controller = require('./controller/Chat_controller.js');
var user_controller = require('./controller/User_controller.js');
var chat_model = require('././model/Chat_model.js');


///////////////ANTERIOR
//var http = require('http');
//var server = app.listen(8081, function () {

//   var host = server.address().address
//   var port = server.address().port
//   console.log('Threads 2017 %s', port)//
//})
//////////////FIN ANTERIOR

//MODIFICADO POR JOHAO PARA PRUEBAS SOCKET

var server = require('http').Server(app);
var io = require('socket.io')(server);

// app.get('/', function (req, res) {
    // res.sendFile(__dirname + '/index.html');
// });

server.listen(8081, function () {
     var host = server.address().address
     var port = server.address().port
    console.log('Threads 2017 %s', port)
});

io.of('/chat').clients(function (error, clients) {
    if (error) throw error;
    console.log(clients); // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]

});

io.on('connection', function (socket) {
    socket.on('chat message', function (msg) {
        io.emit('chat message', msg);
    });
});

/////////////END MODIFICACION

//var io = socketIO.listen(app);



//***==============********  SOCKETS CCC ********===========*********//

// var server = net.createServer();
// server.on('connection', handleConnection);

io.sockets.on('connection', function(socket) {


 // console.log(io.to(socket.id).emit() );
 //console.info('New client connected (id=' + socket.id + ').');
 //console.log(socket.id);
 socket.on('newuser',function(data){

	 console.log(data);

	data.socket_id = socket.id;
	data.status = socket.status;
	console.log("socket recibido");
	console.log(data);

	chat_controller.newuser(data,function(data){

		console.log("socket enviado!!!");

		io.to(socket.id).emit('allchatuser', data);
	});

   //io.to(socket.id).emit('recibemsg', {"msg":"xD"});
 });

//*************Marielb***********//
socket.on('changestatusrooms', function(data){
	
	var dat1 = require('./controller/Chat_controller.js');

	dat1.changestatusrooms(data, function(err, dat3){

		var dat = {"idroom" : data.roomid};
		var mod = require('./model/Chat_model.js');



		mod.getMembersAndSocketsRoom(dat,function(room){
			console.log(room);
			console.log("room");
			if(!room.sockets){return;}
			var unique = room.sockets;

			
			unique.forEach(function(value,index){

				var dat4 = {"roomid" : data.roomid , "error": err , "data": room.data};

				io.to(value).emit('changestatusrooms',  dat4);
				console.log(dat4);
			})
		});




	});
});
//*************Marielb***********//
 socket.on('newmessage', function(data) {
	 console.log("SERVICIO MENSAJE QUE CONSUME WEBSOCKET");
	// console.log(data);

	 if(data.data.insertedIdsmg){
		 var dat = {"_id":data.data.insertedIdsmg};
			chat_controller.getMessageIdAux(dat,function(msg){

			var dat = {"idroom":msg.chat_id};
			var mod = require('./model/Chat_model.js');
			console.log("----primero----"); //ok
			console.log(dat); 

			mod.getMembersAndSocketsRoom(dat,function(room){

				if(!room.sockets){return;}//**Marielb**//


				var unique = room.sockets.filter(function(elem, index, self) { return index == self.indexOf(elem); })
				unique.forEach(function(value,index){

					 io.to(value).emit('alertnewmessage', msg);
				})
			});
		});

	 }else{
		chat_controller.grabarchatcont(data, function(msg){
			console.log("==============================FORMATO DATACCC======================")
			console.log(msg)

			var dat = {"idroom":msg.chat_id};
			var mod = require('./model/Chat_model.js');

			mod.getMembersAndSocketsRoom(dat,function(room){

				if(!room.sockets){return;}//**Marielb**//

				var unique = room.sockets.filter(function(elem, index, self) { return index == self.indexOf(elem); })
				
				unique.forEach(function(value,index){

					 io.to(value).emit('alertnewmessage', msg);
				})
			});
		});
	}



});

 socket.on('newroom',function(dat){


	var data = {"body":dat};
   	var mod = require('./model/ChatAdd_model.js');
   	var mod1 = require('./model/Chat_model.js');
	var rs=mod.ChatAdd(data,null,function(rpta){
		console.log("DATOS ROOMS");
		//console.log(rpta);

		var dat = {"idroom":rpta.data._id};

		io.to(socket.id).emit('newroomresponse', dat);
		console.log("NEWWWWWWWWWW")
		mod1.getMembersAndSocketsRoom(dat,function(room){

			  var newroom = rpta.data;
				//"members": room.members
			  //}
			  newroom.members = room.members;

			  console.log("DATOS noom");
			  console.log(newroom);
			  console.log("*****************room***********************");
			  console.log(room);
			  console.log("*****************room***********************");
			  if(!room || !room.sockets || room.sockets.length < 1){ // se valida que array tenga data y exista
			  	return;
			  }

				room.sockets.forEach(function(value,index){

					io.to(value).emit('alertnewchat', newroom);
				})

		});
	});
});

	 socket.on('getallmessage',function(data){

		  chat_controller.getallmessage(data,function(data){
			  console.log("mensajes!!!");
			  console.log(data);
			  io.to(socket.id).emit('sendallmessage', data);
	 		});


	 });
	 
	 
	 socket.on('disconnect', function() {
         console.log('Se desconectó el usuario!');
		 console.log(socket.id);
		 
		 var dat = {"idsocket":socket.id};
		 
		 chat_controller.deletSockets(dat,function(room){
			 
			 
		 })
  
	 });
	 
	 
});



////////////PERMITIR CONSULTAS DE OTRAS IP

var allowedOrigins = ['http://192.168.10.165:8090','http://192.168.100.9:82', 'http://localhost:62842','http://181.177.235.165', 'http://181.177.235.167:8981'];


var corsOptions = {
    origin: allowedOrigins
}
var issuesoption = {
    origin: true,
    methods: ['POST'],
    credentials: false,
};


app.use(cors(corsOptions))
app.options('*', cors(issuesoption));


///////////////
var firebase = require("firebase");


app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json({limit: '40mb'}));


app.use(bodyParser.json());


var config = {
    apiKey: "AIzaSyBUWv41RRukGhZL2OfilN98tiPxnLdBrss",
    authDomain: "fir-7800d.firebaseapp.com",
    databaseURL: "https://fir-7800d.firebaseio.com",
    storageBucket: "fir-7800d.appspot.com",
    messagingSenderId: "512351056632"
  };

var key='AIzaSyDR6NmOKSfmaIsUi24XPv5gO9Pftds8mFg';

 firebase.initializeApp(config);

  var db = firebase.database();
////////////////////////////////////////////////////////////johao

 /////////////

var request = require("request");
var logcontroller = require('./controller/Validation_controller.js');

app.get('/api/v1/delete', function(req,res){
	 
	 chat_controller.deletSockets(req,res);
})

app.get('/api/v1/getCustomer', function(req,res){
	
	 chat_controller.getCustomers(req,res);
})

app.post('/api/v1/newusuario', function (req, res) {
	 logcontroller.logUserCont(req ,res);

})


app.post('/api/v1/security', function (req, res) {
	 logcontroller.verifsecurityCont(req ,res);
})


app.post('/api/v1/mail-test', function (req, res) {

	console.log("pasa");

var options = { method: 'POST',
 url: 'http://181.177.235.167:8981/mailing/v1/interbank/EnviarMailingMasivo',
 headers:
  {
    'cache-control': 'no-cache',
    'content-type': 'application/json' },
 body:
  { api_key: '454f889ca2031e108f2990c1e5e466c21e55cee6808cd6051684d5b58d84d424',
    data:
     { user: '12345678',
       name_campaign: 'Nombre de campaña de prueba FK',
       subject: 'Envio de prueba FK',
       html: 'texto de prueba :D',
       delimiter: ',',
       fileContent: 'ZnBpbmVkb3QxOUBnbWFpbC5jb20sdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMQ0KZnBpbmVkb0BpbnRpY291c2EuY29tLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjEsdmFyMSx2YXIxLHZhcjE=',
       remitente: { correo: 'fpinedot19323@intico.com.pe', nombre: 'FK2' } } },
 json: true };

request(options, function (error, response, body) {
 if (error) throw new Error(error);

 console.log(body);
  res.send(JSON.stringify(body));
});

})

app.post('/api/v1/sms-test', function (req, res) {

var options = { method: 'POST',
 url: 'http://181.177.235.167:8980/ApiSms/api/sms_bulk',
 headers:
  {
    'cache-control': 'no-cache',
    'content-type': 'application/json' },
 body:
{
"api_key" : "J274bMdKCjx/J3JkHdglXGOjs3NpnMz5k3EfjuQCyZw=",
"data":{
	"messages" :[ {
	"phone" : "980534431" ,
	"message" : "prueba de johao premium api masivo"
	}],
	"hour" : "15:21:10",
	"delivery_date" : "07/20/2017"
	,
	"user" : {
		"user_id" : 16147,
		"country_code" : "PE"
	}
}
},
 json: true };

request(options, function (error, response, body) {
 if (error) throw new Error(error);

 console.log(response);
  res.send(JSON.stringify(body));
});

})

///////////////////
app.post('/api/v1/user-update', function (req, res) {

	//console.log(req.body)
   var con = require('./controller/User_controller.js');
  var rs=con.ValidarSessionUser(req,res,userUpdate);

})


function userUpdate(items,req,res){
	var con = require('./controller/User_controller.js');
  var rs=con.userUpdate(req,res);

}
/////////////////////////////////////////////////// end johao

app.get('/api/v1/getPerson', function (req, res) {
    var mod = require('./model/Chat_model.js');
    var rs = mod.getPerson(req, res);
});

app.post('/api/v1/chat2', function(req, res) {

	if ((typeof req.body.idReceptor == "undefined" || req.body.idReceptor == null)
		&& (typeof req.body.mensaje == "undefined" || req.body.mensaje == null)
		&& (typeof req.body.secureObjKey == "undefined" || req.body.secureObjKey == null)
		&& (typeof req.body.apikey == "undefined" || req.body.apikey == null) )

	 var idReceptor =  req.body.idReceptor
	 var mensaje =  req.body.mensaje
	 var secureObjKey = req.body.secureObjKey
	 var apikey =  req.body.apikey

	rs=ValidarAPikey(apikey)

	console.log(JSON.stringify(rs['respCod']));
	 if(JSON.stringify(rs['respCod'])=='"1"'){
		res.end( JSON.stringify(rs) );
	}


db.ref("appName").on("value",function(snapshot){
  res.end( JSON.stringify(snapshot.val()) );

})


})

app.post('/api/v1/chat-list-main-account', function (req, res) {
    var mod = require('./model/ChatUser_model.js');
    var rs = mod.getAccountMonth(req, res);
})
//******************************************//

app.post('/api/v1/user-get-mother-account', function (req, res) {
    var mod = require('./model/ChatUser_model.js');
    var rs = mod.getAccountMonth(req, res);
})


app.post('/api/v1/chat-list-filter', function (req, res) {
  var mod = require('./model/ChatUser_model.js');
  var rs = mod.searchUser(req, res);
})

app.post('/api/v1/filter_useritk',user_controller.filter_useritk); //ccc

app.post('/api/v1/user-get-by-extension', function(req, res){

	 var  mod = require('./model/session_model.js');
	 var rs = mod.userbyExtension(req,res);

})


app.post('/api/v1/customer-add', function (req, res, a) {

    var db = require('mongodb').MongoClient;

    var user = req.body;
    var name = req.body.name;
    var email = req.body.email;
    req.body.apikey = getSha2(name + email);

    db.connect(urlDB, function (err, db) {
        if (err) { return console.dir(err); }

        db.collection('customer', { safe: true }, function (err, collection) {

            collection.find({ "name": name }).toArray(function (err, items) {
                x = items;

                if (items == null || items.length == 0) {
                    collection.insert(user, function (err, doc) {
                        if (err) {
                            console.log("Error on document insert");

                            res.end(JSON.stringify("Error on document insert"))

                        } else {
                            console.log("Document saved succesfuly");

                            res.end(JSON.stringify("Document saved succesfuly"))

                        }
                    });
                }
                else {
                    res.end(JSON.stringify("Existe Cliente"))
                }
            }
             );

        });

    });

});


var Respuesta = function (respCod, respMsg, data) {
    this.respCod = respCod;
    this.respMsg = respMsg;
    this.data = data;

};

app.post('/api/v1/session-update', function (req, res) {
	console.log("----UpdateSession----")
	var mod = require ('./model/session_model.js');
	var rs = mod.updateSession(req,res);

})


app.post('/api/v1/chat-add', function (req, res) {
	var mod = require('./model/ChatAdd_model.js');

	var fullUrl = req.protocol + '://' + req.get('host') + req.originalUrl;

	console.log("aaaaaaaaaaaa");
	console.log(fullUrl);
	console.log("aaaaaaaaaaaa");

	var rs=mod.ChatAdd(req,res);

})


app.post('/api/v1/user-update-id-device', function (req, res) {
    var mod = require('./model/ChatUser_model.js');
    var rs = mod.updateUser(req, res);
})


app.post('/api/v1/session-add', function (req, res, a) {
	var mod = require('./model/Chat_model.js');
  var rs=mod.SessionAdd(req,res);
})


app.post('/api/v1/chat', function (req, res) {
	console.log("SERVICIO MENSAJE QUE CONSUME LA APP");
	 var mod = require('./controller/Chat.js');
	 var rs=mod.ValidarSessionUser(req,res,GrabarChat);
})

function GrabarChat(items,req,res){
	var con = require('./controller/Chat.js');
	req.body.canalfk='apk'
	var rs=con.GrabarChat(req,res);

}


//------------------TALKS ----------------------

//-Broadcast message
	 // app.route('/api/v1/broadcast')
	 // .post(mod.grabarBroadcastMessage);
app.post('/api/v1/broadcast', function (req, res) {
	 var mod = require('./model/Broadcast_model.js');
	 var rs=mod.grabarBroadMessage(req,res);
})

//--Broadcast Add

	 // app.route('/api/v1/broadcast-add')
	 // .post(mod.broadcastAdd);
app.post('/api/v1/broadcast-add', function (req, res, a) {
	 var mod = require('./model/Broadcast_model.js');
//console.log(res);
    var rs=mod.broadcastAdd(req,res);
})

//----Broadcast update

app.post('/api/v1/broadcast-update', function (req, res) {
	var con = require('./controller/Chat.js');

	var rs=con.ValidarSessionUser(req,res,ChatBroadUpdate);

})
function ChatBroadUpdate(items,req,res){
	var con = require('./controller/Chat.js');
//console.log(items);

var rs=con.BroadcastUpdate(req,res);

}


app.post('/api/v1/broadcast-suscribe', function (req,res){
	var con = require('./controller/Chat.js');
	//console.log(req);
	var rs=con.ValidarCustomer(req,res,broadcastSuscribe);

})
function broadcastSuscribe(req,res){
	var con = require('./controller/Chat.js');
	console.log("Controller Chat" + req.body.data);
	var rs=con.broadcastSuscribe(req,res);

}

app.post('/api/v1/broadcast-unsuscribe', function (req,res){
	var con = require('./controller/Chat.js');
	var rs=con.ValidarCustomer(req,res,broadcastUnsuscribe);


})
function broadcastUnsuscribe(req,res){
	var con = require('./controller/Chat.js');
	//var mod = require('./model/Chat_model.js');
	var rs=con.broadcastUnsuscribe(req,res);
}

app.post('/api/v1/broadcast-remove', function (req,res){
	var con = require('./controller/Chat.js');
	var rs=con.ValidarCustomer(req,res,broadcastRemove);
})

function broadcastRemove(req,res){
	var con = require('./controller/Chat.js');
	//var mod = require('./model/Chat_model.js');
	var rs=con.broadcastRemove(req,res);
}


// --------------------- END Talks ------------------

app.post('/api/v1/chat-get-information', function (req, res) {
	var con = require('./controller/Chat.js');
//console.log(req.body)
	var rs=con.ValidarSessionUser(req,res,ChatGetInformation);

})

function ChatGetInformation(items,req,res){
	var con = require('./controller/Chat.js');
//console.log(items);

var rs=con.ChatGetInformation(req,res);

}


app.post('/api/v1/chat-update', function (req, res) {
	var con = require('./controller/Chat.js');
//console.log(req.body)
	var rs=con.ValidarSessionUser(req,res,ChatUpdate);

})

function ChatUpdate(items,req,res){
	var con = require('./controller/Chat.js');
//console.log(items);

var rs=con.GenerateImageChatUpdate(req,res);

}


app.post('/api/v1/broadcast-update', function (req, res) {
	var con = require('./controller/Chat.js');
//console.log(req.body)
	var rs=con.ValidarSessionUser(req,res,BroadUpdate);

})

function BroadUpdate(items,req,res){
	var con = require('./controller/Chat.js');
//console.log(items);

var rs=con.BroadcastUpdate(req,res);

}

app.post('/api/v1/chat-typing', function (req, res) {
//console.log(req.body);
//console.log('--------------');

var fir = require('./model/Push_notifications_model.js');
var push=fir.ChatTyping(req,res);

})


app.post('/api/v1/user-add', function (req, res, a) {

    var mod = require('./model/ChatUser_model.js');
	if(req.body.data.extension && req.body.data.id_device){ //Usuarios italkyou
		 console.log("Usuario italkyou");
		 // console.log(req.body);
		 var rs = mod.addUser(req, res);

	}else if (req.body.data.extension && !(req.body.data.id_device)){ //Usuario Ejec CCC

		 console.log("Usuario Ejec CCC");
		 // console.log(req.body);
		 var rs = mod.addUserityEjecuCCC(req,res);

	}else if(!(req.body.data.extension) && !(req.body.data.id_device) ){  //Usuarios externos sockets
		 console.log("Usuario externos sockets");
		  //console.log(req.body);
		 var rs = mod.addUserWeb(req, res);
	}
})

app.post('/api/v1/bulkWriteUser', function (req, res) {

    var mod = require('./model/ChatUsers_model.js');
    var rs = mod.bulkWriteUser(req, res);
})


app.post('/api/v1/updateuserdateCreation', function (req, res) {

    var mod = require('./model/User_model.js');
    var rs = mod.UpdateUserDateCreation(req, res);
})


app.post('/api/v1/contact-sync', function (req, res) {

    var mod = require('./model/ChatUsers_model.js');
    var rs = mod.syncContacts(req, res);
})


app.post('/api/v1/chat-list-start', function (req, res) {

    var mod = require('./model/ChatUsers_model.js');
    var rs = mod.getSyncContacts(req, res);
})


app.post('/api/v1/add-id-device', function (req, res) {
    var mod = require('./model/ChatUser_model.js');
    var rs = mod.agregarIdDevice(req, res);

})


app.post('/api/v1/call-incoming-video', function (req, res) {
    var con = require('./controller/Chat.js');
    var rs = con.ValidarSessionUser(req, res, callIncoming);

})

app.post('/api/v1/call-incoming2', function (req, res) {
    var con = require('./controller/Sip_controller.js');
    var rs = con.callIncoming(req, res, callIncomingVideo);

})

// app.post('/api/v1/call-incoming', function (req, res) {
    // var con = require('./controller/Chat.js');
    // var rs = con.ValidarSessionUser(req, res, callIncomingVideo);

// })

function callIncomingVideo(kkk, req, res) {

    var con = require('./controller/Sip_controller.js');
    var rs = con.callIncoming(req, res);
}

function callIncoming(kkk,req, res) {

    var con = require('./controller/Sip_controller.js');
    var rs = con.callIncoming(req, res);
}

//ESTADISTICAS

app.post('/api/v1/user-count', function (req, res) {
    var con = require('./model/Estadisticas_model.js');
    var rs = con.GetUSerCount(req, res);

})

app.post('/api/v1/get-chat-by-month', function (req, res) {
    var con = require('./model/Estadisticas_model.js');
    var rs = con.GetChatByMonth(req, res);

})

app.post('/api/v1/get-chat-count-by-user', function (req, res) {
    var con = require('./model/Estadisticas_model.js');

    var rs = con.GetChatCountByUser(req, res);

})

app.post('/api/v1/user-count-by-month', function (req, res) {
    var con = require('./model/Estadisticas_model.js');

    var rs = con.GetUSerCountByDate(req, res);

})

//END ESTADISCTICAS

app.get('/public/model/img/:file', function (req, res) {
    file = req.params.file;
    var img = fs.readFileSync(__dirname + "/model/img/" + file);
    res.writeHead(200, { 'Content-Type': 'image/png' });
    res.end(img, 'binary');
});

app.get('/public/model/profile/:file', function (req, res) {
    file = req.params.file;


	fs.exists(__dirname + "/model/profile/" + file, function(exists) {
	if (exists) {
		   var img = fs.readFileSync(__dirname + "/model/profile/" + file);
		   res.writeHead(200, { 'Content-Type': 'image/png' });
		   res.end(img, 'binary');
	}
 });

});


/*marielb*/

/////////////////////MARIELB//////////////////////
var urltopic = "/api/v1/";
var topic_controller = require('./controller/topic_controller.js');
var task_controller = require('./controller/task_controller.js');
var file_controller = require('./controller/file_controller.js');
var tag_controller = require('./controller/tag_controller.js');
var ccc_controller = require('./controller/ccc_controller.js');

app.route(urltopic+'getroombystatus')
.post(chat_controller.getroombystatus);

app.route(urltopic+'topic-add')
.post(topic_controller.topic_add);

app.route(urltopic+'topic-get/')
.post(topic_controller.topic_get);

app.route(urltopic+'topic-related')
.post(topic_controller.topic_related);

app.route(urltopic+'topic-attach-file')
.post(topic_controller.topic_attach_file);

app.route(urltopic+'topic-update-users')
.post(topic_controller.topic_update_users);

app.route(urltopic+'topic-update-general')
.post(topic_controller.topic_update_general);

app.route(urltopic+'topic-comment')
.post(topic_controller.topic_comment);

app.route(urltopic+'comment-get-topic')
.post(topic_controller.comment_get_topic);

app.route(urltopic+'topic-suscribe')
.post(topic_controller.topic_suscribe);

app.route(urltopic+'topic-get-user')
.post(topic_controller.topic_get_user);

app.route(urltopic+'topic-device')
.post(topic_controller.topic_device);

app.route(urltopic+'topic-history')
.post(topic_controller.topic_history);

app.route(urltopic+'comment-topic-history')
.post(topic_controller.comment_topic_history);

app.route(urltopic+'task-add')
.post(task_controller.task_add);

app.route(urltopic+'task-comment')
.post(task_controller.task_comment);

app.route(urltopic+'comment-get-task')
.post(task_controller.comment_get_task);

app.route(urltopic+'task-mark')
.post(task_controller.task_mark);

app.route(urltopic+'task-get')
.post(task_controller.task_get);

app.route(urltopic+'task-update')
.post(task_controller.task_update);

app.route(urltopic+'task-history')
.post(task_controller.task_history);

app.route(urltopic+'comment-task-history')
.post(task_controller.comment_task_history);

app.route(urltopic+'file-upload')
.post(file_controller.file_upload);

app.route(urltopic+'file-get')
.post(file_controller.file_get);

app.get(urltopic+'assets_topic/:file', function (req, res) {

	var file = req.params.file;
    var aux = file.split('.');
    var img = fs.readFileSync(__dirname + "/assets/topic/" + file);
    console.log("--------partiendo");
    console.log(aux);

    if(aux[1] == "png"){
    	res.writeHead(200, { 'Content-Type': 'image/png' });
    }else if(aux[1] == "pdf"){
    	res.writeHead(200, { 'Content-Type': 'application/pdf' });
    }else if(aux[1] == "mp3"){
    	res.writeHead(200, { 'Content-Type': 'audio/mp3' });
    }else if(aux[1] == "mp4"){
    	res.writeHead(200, { 'Content-Type': 'audio/mp4' });
    }else if(aux[1] == "doc"){
    	res.writeHead(200, { 'Content-Type': 'application/msword' });
    }else if(aux[1] == "rar"){
    	res.writeHead(200, { 'Content-Type': 'application/x-rar-compressed' });
    }else if(aux[1] == "zip"){
    	res.writeHead(200, { 'Content-Type': 'application/zip' });
    }

    console.log("----file-----");
    console.log(file);
    res.end(img, 'binary');
});



/*marielb*/


/*Omnicanalidad campaña */
var rutaomnicanlidad = express.Router();
var customer = require('./controller/campana_controller.js');
 rutaomnicanlidad.route('/campana')
   //.get(customer.Campana)
   .post(customer.RegisterCampana)
   //.delete(customer.DeleteCampana)
   //.put(customer.UpdateCampana);
app.use('/api/v1', rutaomnicanlidad);
/*Omnicanalidad campaña */



/*servicios omnicalidad */
var serviciomnicanlidad = express.Router();
var servicio = require('./controller/servicios_omni.js');
 serviciomnicanlidad.route('/services/status')
   //.get(servicio.Campana)
   .post(servicio.EstadosServicios)
   //.delete(servicio.DeleteCampana)
   //.put(servicio.UpdateCampana);
app.use('/api/v1', serviciomnicanlidad);
/*servicios omnicalidad */


/*Customer */
var rutacustomer = express.Router();
var customer = require('./controller/customer_controller.js');
 rutacustomer.route('/customer')
   //.get(customer.getCustomer)
   .post(customer.RegisterCustomer)
   //.delete(customer.DeleteClient)
   //.put(customer.UpdateClient);
app.use('/api/v1', rutacustomer);
/*Customer */


/*uduario omni*/
 var rutausuario = express.Router();
var userOmni = require('./controller/userOmni.js');
 rutausuario.route('/users')
   .get(userOmni.Users)
   .post(userOmni.RegisterUsers)

 rutausuario.route('/users/:id')
   .get(userOmni.UsersCod)
   .delete(userOmni.DeleteUsers)
   .put(userOmni.UpdateUsers);



app.use('/api/v1', rutausuario);

/*usuario omni*/


/*Mailing FK*/

 var rutafk = express.Router();
var mailing = require('./controller/Mailing.js');
var suscribers = require('./controller/suscribers_controller.js');


  rutafk.route('/mailing/sendbulk')
   //.get(mailing.Client)
   .post(mailing.SendmailingBulk)
  // .delete(mailing.DeleteClient)
   //.put(mailing.UpdateClient);

/*
   app.post('/api/v1/mailing/sendbulk', function (req, res) {
	    console.log(req.body)

	  // process.exit()
   });
   */

rutafk.route('/mailing/send')
   //.get(mailing.Client)
   .post(mailing.Sendmailing)
  // .delete(mailing.DeleteClient)
   //.put(mailing.UpdateClient);
   
   
 rutafk.route('/mailing/boletin')
   .get(mailing.Boletin)
   .post(mailing.RegisterBoletin)
	


 rutafk.route('/mailing/boletin/:id')
   .get(mailing.BoletinCod)
   //.delete(userOmni.DeleteUsers)
   //.put(userOmni.UpdateUsers);
	
 rutafk.route('/subscribers')
   .get(suscribers.ListarAllSuscribers)
   .post(suscribers.RegistrarOneSuscriber)
   .patch(suscribers.RegistrarFileSuscriber)
	


 rutafk.route('/subscribers/:id')
   .get(suscribers.ListarCodeSuscribers)
   .delete(suscribers.DeleteOneSuscriber)
   .put(suscribers.ActualizarOneSuscriber);
	
	
	
	
app.use('/api/v1', rutafk);	

/*Mailing FK*/
app.use('/api/v1', rutafk);
/*sms*/

var ruta = express.Router();
var sms = require('./controller/sms_controller.js');
ruta.route('/sms/send')
 //.get(sms.sendsms)
  .post(sms.sendsms)
// .delete(sms.sendsms)
//  .put(sms.sendsms);

ruta.route('/sms_bulk/send')
 //.get(sms.sendsms)
  .post(sms.send_sms_bulk)
// .delete(sms.sendsms)
//  .put(sms.sendsms);

app.use('/api/v1', ruta);

/* fin sms*/

// app.post('/api/v1/sms/send', function (req, res) {

	// console.log(req.body);


// });








//------------
app.get('/webhook', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === 'frank_pinedo_terrones') {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);
  }
});

app.get('/webhook2', function(req, res) {
  if (req.query['hub.mode'] === 'subscribe' &&
      req.query['hub.verify_token'] === 'frank_pinedo_terrones') {
    console.log("Validating webhook");
    res.status(200).send(req.query['hub.challenge']);
  } else {
    console.error("Failed validation. Make sure the validation tokens match.");
    res.sendStatus(403);
  }
});



app.post('/webhookfk', function (req, res) {
   var data = req.body;
   console.log(data);
      var pageID = data.id;
    receivedMessage(data.event);
    res.sendStatus(200);

 });

 app.post('/webhook', function (req, res) {
	 console.log('--------');
   var con = require('./controller/Bootfk.js');
   var rs = con.webhook(req, res);

 });


  app.post('/api/v1/facebook/Client', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.Client(req, res);
   });



  app.post('/api/v1/facebook/Register/Client', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.RegisterClient(req, res);
   });

  app.post('/api/v1/facebook/Update/Client', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.UpdateClient(req, res);
   });

  app.post('/api/v1/facebook/Delete/Client', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.DeleteClient(req, res);
   });


   app.post('/api/v1/facebook/Register/Message', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.RegisterMessage(req, res);
   });

  app.post('/api/v1/facebook/Update/Message', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.UpdateMessage(req, res);
   });

  app.post('/api/v1/facebook/Delete/Message', function (req, res) {
   var con = require('./controller/Bootfk.js');
   var rs = con.DeleteMessage(req, res);
   });



//------------
var https = require('https');
var fs = require('fs');

var options = {
 key: fs.readFileSync('/etc/letsencrypt/live/threads.italkyou.com/privkey.pem'),
 cert: fs.readFileSync('/etc/letsencrypt/live/threads.italkyou.com/cert.pem'),
 ca: fs.readFileSync('/etc/letsencrypt/live/threads.italkyou.com/chain.pem')
};

https.createServer(options, app).listen(443);
// //PM2
// app.use(pmx.expressErrorHandler());
